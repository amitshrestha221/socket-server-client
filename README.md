### PNDC Socket Server & Client Connection Complete

- Socket server based on Node.
- Simple PNDC chat application.
- Socket Client integration for Laravel application.



### Socket Server Setup

- First clone the repo anywhere on the server whereever you find feasible.
- Install NPM Packages.

	`$ npm install`

- After the setup , traverse your terminal inside the cloned directory and  run :

	`$ node socket_server.js`

	This will run the project as localhost:3000 or respective domain:3000, the domain and port can be configured inside the socket_server.js file.

- PNDC chat application will popup after that you can test it from localhost:3000.

- Socket server is complete.

### Laravel Client Setup

- From the cloned directory, copy laravel_side/socket_client.js inside the root of your laravel project.
- Install NPM Packages.

	`$ npm install`
	
	`$ npm install --save socket.io-client`

- If you look carefully inside the socket_client.js file, you have to replace two parameters : socketpath and http request hostname.
- For Laravel application to constantly listen to the socket server run inside your project directory:

	`$ node socket_client.js`

- Your laravel project will start listening to the socket server now.
- Copy the route from laravel_side/web.php to your routes OR paste following to your routes.

```
    <?php
        	Route::post('laravel-event', function(){
					event(new \App\Events\SocketFireEvent(request()->all()));
			});
    ?>
```    
    
- Add the route 'laravel-event' 	 to your except array in app/Http/Middlewares/VerifyCsrfToken.php


```php
    <?php
        	protected $except = [
					'laravel-event'
			];
    ?>
```

- Copy the laravel_side/SocketFireEvent.php to your app/Events/
- If any incoming messages occurs, the socket client will now point to the __construct() of your SocketFireEvent event class. You can do anything there as per your need.

### Laravel Client View Setup
- You can listen to the socket server from the view using following code.

```html

<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js"></script>
<script>
    //var socket = io('http://localhost:3000');
    var socket = io.connect('http://localhost:3000');

    socket.emit('new_message', {message : 'This is test message from laravel application', username : 'Hello'});

</script>

```

**Thank you**